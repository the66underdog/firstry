//For **
const arrow_lefty_1 = document.querySelector("#l-1");
const arrow_righty_1 = document.querySelector("#r-1");
const arrow_lefty_2 = document.querySelector("#l-2");
const arrow_righty_2 = document.querySelector("#r-2");

const list_1 = document.getElementById("1st");
const list_2 = document.getElementById("2nd");
let list_all;
//For hwjs-07
let smLections = document.querySelector(".sub-menu-lections");
let smLectionsContent = smLections.innerHTML;
smLections.innerHTML = '';
let arrL = document.querySelector('.arrow-lections');

let smContacts = document.querySelector(".sub-menu-contacts");
let smContactsContent = smContacts.innerHTML;
smContacts.innerHTML = '';
let arrC = document.querySelector('.arrow-contacts');
//For hwjs-08
let arr = [];
let filterBtn = [];
let filteRd = [];
//For hwjs-09
const obj = {};
//** 
function scrollLecL(theId) {
    document.getElementById(theId).scrollBy(-list_1.clientWidth / 4,0);
}

function scrollLecR(theId) {
    document.getElementById(theId).scrollBy(list_1.clientWidth / 4,0);
}

function subMenu (smObj, smCont, smArrow) {
    let par = smObj.parentElement;
    let smContent = smObj.innerHTML;
    smObj.style.animation = 'none';
    smObj.offsetHeight;
    if (par.open == false) {
        smObj.innerHTML = smCont;
        smObj.style.cssText = `
            animation: pop-up 1s;
        `;
        smArrow.style.cssText = `
            transform: rotate(-180deg);
            transition-duration: 0.66s;
        `;
    }
    else {
        smObj.innerHTML = '';
        smArrow.style.cssText = `
        transition-duration: 0.66s;
        `;
    }
}
//hwjs-08
function sectionCreator(sec) {
    let mainList = document.querySelectorAll('.main-list ul li');
    let i = 0;
    for (let mainListItem of mainList) {
        arr[i] = mainListItem.cloneNode(true);
        i++;
    }
    const allLec = document.createElement('section');
    allLec.className = 'all-lections';
    allLec.innerHTML = sec.parentElement.innerHTML;
    let txtUl = document.createElement('ul');
    for (let cont of allLec.childNodes) {

        if (cont.className == 'main-list-header') {
            cont.innerHTML = `<h2>ВСЕ ЛЕКЦИИ НА НАШЕМ САЙТЕ</h2>
            <h1>ALL LECTURES/</h1>`;
        }

        if (cont.className == 'main-list') {
            for (i = 0; i < arr.length; i++) {
                txtUl.innerHTML += arr[i].outerHTML;
            }
            cont.innerHTML = txtUl.outerHTML;
            cont.id = 'all'
            list_all = cont;
        }
        if (cont.className == 'main-list-scroll') {
            let fl = 0;
            allLecBtn = cont.firstElementChild;
            allLecBtn.innerHTML = `${arr.length} лекций`;
            allLecBtn.style.marginLeft = 'calc(0.5rem + 0.21vw)';
            allLecBtn.addEventListener("mousedown", filter);
            filterBtnCreator(fl, 'html');
            filterBtnCreator(fl+1, 'css');
            filterBtnCreator(fl+2, 'js');

            const arrow_lefty_all = cont.lastElementChild.firstElementChild;
            const arrow_righty_all = cont.lastElementChild.lastElementChild;
            arrow_lefty_all.id = "l-all";
            arrow_righty_all.id = "r-all";
            arrow_lefty_all.addEventListener("mousedown", () => scrollLecL('all'));
            arrow_righty_all.addEventListener("mousedown", () => scrollLecR('all'));
        }
    }
    sec.parentElement.before(allLec);
}

function filterBtnCreator(i, dtGrp) {
    filterBtn[i] = allLecBtn.cloneNode();
    filterBtn[i].id = dtGrp;
    filterBtn[i].innerHTML = dtGrp.toUpperCase();
    allLecBtn.before(filterBtn[i]);
    filterBtn[i].addEventListener("mousedown", () => filter(dtGrp));
}

function filter(dtGp) {
    let i = 0;
    filteRd = [];
    if (dtGp !== 'html' && dtGp !== 'css' && dtGp !== 'js') {
        for (i; i < arr.length; i++) {
            filteRd[i] = arr[i];
        }
    }
    else {
        i = 0;
        for (let y = 0; y < arr.length; y++) {
            if (arr[y].dataset.group == dtGp) {
                filteRd[i] = arr[y];
                for (let filteredItem of filteRd[i].firstElementChild.children) {
                    if (filteredItem.className == 'list-element-info') {
                        filteredItem.firstElementChild.innerHTML = `${dtGp.toUpperCase()}`;
                    }
                }
                i++;
            }
        }
    }
    let txtUl = document.createElement('ul');
    for (i = 0; i < filteRd.length; i++) {
        txtUl.innerHTML += filteRd[i].outerHTML;
    }
    document.querySelector('#all').innerHTML = txtUl.outerHTML;
}
//hwjs-09
function objFiller() {
    for (let ar of arr) {
        const objInto = {};
        for (let a of ar.firstElementChild.children) {
            if (a.tagName == "IMG") {
                objInto["image"] = a.src;
            }
            if (a.className == "content-type") {
                objInto["label"] = a.innerHTML;
            }
            if (a.className == "list-element-info") {
                objInto["title"] = a.firstElementChild.innerHTML;
                objInto["description"] = a.lastElementChild.innerHTML;
            }
            if (a.className == "list-element-date") {
                objInto["date"] = a.firstElementChild.innerHTML;
            }
        }
        if (!(`${ar.dataset.group}` in obj)) {
            obj[`${ar.dataset.group}`] = [];
        }
        obj[`${ar.dataset.group}`].push(objInto);
    }
    console.log(obj);
}
//Listeners
arrow_lefty_1.addEventListener("mousedown", () => scrollLecL('1st'));
arrow_righty_1.addEventListener("mousedown", () => scrollLecR('1st'));
arrow_lefty_2.addEventListener("mousedown", () => scrollLecL('2nd'));
arrow_righty_2.addEventListener("mousedown", () => scrollLecR('2nd'));
smLections.previousElementSibling.addEventListener("click", 
() => subMenu(smLections, smLectionsContent, arrL));
smContacts.previousElementSibling.addEventListener("click", 
() => subMenu(smContacts, smContactsContent, arrC));
//Onload
window.onload = function () {
    sectionCreator(document.querySelector('.main-list-header'));
    objFiller();
}
