/* Пункт 1 */
let textSqc = document.querySelectorAll('h3');
for (let res of textSqc) {
    textRes = res.innerHTML;
    res.innerHTML = textRes.toUpperCase();
}

/* Пункт 2 */
textSqc = document.querySelectorAll('.list-element-info span');
for (res of textSqc) {
    textRes = res.innerHTML;
    if(textRes.length > 20) { 
        res.innerHTML = textRes.slice(0,20) + '...';
    }
}